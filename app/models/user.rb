class User < ApplicationRecord
	
   require 'bcrypt'

   has_secure_password

   has_many :projects

   validates_uniqueness_of :email_id, :message => "Already Exists"

   def user_full_name
    	"#{first_name} #{last_name}"
   end

end
