class SessionsController < ApplicationController
  skip_before_action :check_session, only:[:login, :login_process]


  def login
	render :layout => false
  end

  def login_process
	user = User.find_by(email_id: params["email_id"])
	if user && user.authenticate(params["password"])
		session[:user_id] = user.id
		redirect_to welcome_user_path
	else
		redirect_to root_url, alert: "Invalid Username or Password"
	end
  end

  def logout
	session[:user_id] = nil
  	redirect_to login_path
  end

  def welcome_user
	@user = current_user
  end

end
